import { Component, OnInit, Renderer, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/service.index';
import { Usuario } from '../models/usuario.model';

declare function init_plugins();
declare var toastr: any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit, OnDestroy {

  usuario: string;
  clave: string;


  constructor(public router: Router
    ,         public USUARIO_SERVICE: UsuarioService) {
  }



  ngOnInit() {
   init_plugins();
  }

  ingresar(forma: NgForm) {

    if (forma.invalid) {
      return;
    }
    const usuario = new Usuario( forma.value.usuario, forma.value.clave);

    this.USUARIO_SERVICE.login(usuario).subscribe(
      correcto => {
        if (correcto) {
          this.router.navigate(['/dashboard']);
        } else {
          // toastr.error('error', 'Login');
        }
      }
    );



  }


  ngOnDestroy() {
   //  this.renderer.setElementClass(document.body, 'login-page', false);
  }
}
