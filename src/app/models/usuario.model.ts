export class Usuario {

    constructor(
        public usuario: string,
        public password: string,
        public origen?: string,
        public apeMaterno?: string,
        public apePaterno?: string,
        public nombres?: string,
        public opciones?: string
        ) {
    }

}
