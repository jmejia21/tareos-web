

import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'payments'
})
export class PaymentsPipe implements PipeTransform {

  transform(array: any[], filter: string): any {
    filter = filter ? filter.toLocaleLowerCase() : '';
      return filter && array ?
        array.filter(invoice =>
           (invoice.customerRuc !== null  ? invoice.customerRuc.toLocaleLowerCase().indexOf(filter) !== -1 : false)  ||
           (invoice.customerName !== null  ? invoice.customerName.toLocaleLowerCase().indexOf(filter) !== -1 :  false) ||
           (invoice.currency.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.amountTotal.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.amountBalance.toString().toLocaleLowerCase().indexOf(filter) !== -1)
        ) :        array;
  }

}
