
import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'invoice'
})
export class InvoicePipe implements PipeTransform {

  transform(array: any[], filter: string): any {
    filter = filter ? filter.toLocaleLowerCase() : '';
      return filter && array ?
        array.filter(invoice =>
           (invoice.customerName !== null  ? invoice.customerName.toLocaleLowerCase().indexOf(filter) !== -1 : false)  ||
           (invoice.documentId !== null  ? invoice.documentId.toLocaleLowerCase().indexOf(filter) !== -1 :  false) ||
           (invoice.dueDateFormat.toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.due.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.amountTotal.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.status.toLocaleLowerCase().indexOf(filter) !== -1)
        ) :        array;
  }

}
