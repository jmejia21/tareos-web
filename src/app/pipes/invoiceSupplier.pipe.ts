

import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'invoiceSupplier'
})
export class InvoiceSupplierPipe implements PipeTransform {

  transform(array: any[], filter: string): any {
    filter = filter ? filter.toLocaleLowerCase() : '';
      return filter && array ?
        array.filter(invoice =>
           (invoice.customerRuc !== null  ? invoice.customerRuc.toLocaleLowerCase().indexOf(filter) !== -1 : false)  ||
           (invoice.customerName !== null  ? invoice.customerName.toLocaleLowerCase().indexOf(filter) !== -1 :  false) ||
           (invoice.nroComprobante.toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.documentDateFormat.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.dueDateFormat.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.currency.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.amountTotal.toString().toLocaleLowerCase().indexOf(filter) !== -1) ||
           (invoice.amountBalance.toString().toLocaleLowerCase().indexOf(filter) !== -1)
        ) :        array;
  }

}
