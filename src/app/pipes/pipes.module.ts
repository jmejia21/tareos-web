import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicePipe } from './invoice.pipe';
import { InvoiceSupplierPipe } from './invoiceSupplier.pipe';
import { PaymentsPipe } from './payments.pipe';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    InvoicePipe,
     InvoiceSupplierPipe ,
     PaymentsPipe
    ],
  exports: [
    InvoicePipe ,
    InvoiceSupplierPipe,
    PaymentsPipe
  ]
})
export class PipesModule { }
