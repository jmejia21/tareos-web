import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UsuarioService } from '../../services/service.index';

declare var jquery: any;
declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-trabajadores',
  templateUrl: './trabajadores.component.html',
  styles: []
})
export class TrabajadoresComponent implements OnInit {

  public trabajadores = [];
  public dataTable: any;

  constructor(public USUARIO_SERVICE: UsuarioService, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
     this.getTrabajadores();
  }

  getTrabajadores() {
    this.USUARIO_SERVICE.getWorkers().subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          $('#table_trabajadores').DataTable().destroy();
          this.trabajadores = resp.trabajadores;
          this.chRef.detectChanges();
          const table: any = $('#table_trabajadores');
          this.dataTable = table.DataTable({
              language: {
                search: 'Buscar:',
                lengthMenu: 'Mostrar   _MENU_   trabajadores',
                zeroRecords: 'No hay trabajadores',
                info: 'Mostrar página _PAGE_ de _PAGES_',
                infoEmpty: 'No hay trabajadores disponibles',
                infoFiltered: '(filtrado desde _MAX_ registros)',
                paginate: {
                  previous: 'Anterior',
                  next: 'Siguiente'
                }
              }
          });

        } else {
          console.log(resp);
        }
      }
    );
  }
}
