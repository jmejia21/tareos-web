import { Injectable,  EventEmitter } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
declare var jquery: any;
declare var $: any;
declare var toastr: any;

@Injectable()
export class ConfirmDialogService {

    private subject = new Subject<any>();
    private show: false;
    public notificacion = new EventEmitter<any>();

    constructor() {
    }
    async confirm(): Promise<boolean> {
        // const confirmation = window.confirm(message || 'Are you sure?');
        // return of(confirmation);
        // let confirmation = false;

        const modal = $('#modalConfirmPage').modal();
        modal.modal({backdrop: 'static', keyboard: false});
        // tslint:disable-next-line:no-unused-expression
        return new Promise<boolean>((resolve, reject) => {
            document.getElementById('btnAccept').onclick = (() => {
                $('#modalConfirmPage').modal('hide');
                resolve(true);
            });
            document.getElementById('btnCancel').onclick = (() => {
                $('#modalConfirmPage').modal('hide');
                resolve(false);
            });
        });

      }
}
