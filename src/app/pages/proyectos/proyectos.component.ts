import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UsuarioService, ProyectoService } from '../../services/service.index';

declare var jquery: any;
declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styles: []
})
export class ProyectosComponent implements OnInit {

  public proyectos = [];
  public dataTable: any;
  public proyectoSeleccionado: any;

  constructor(public USUARIO_SERVICE: UsuarioService, public PROYECTO_SERVICE: ProyectoService, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getProyectos();
  }

  getProyectos() {
    this.USUARIO_SERVICE.getProjects().subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          $('#table_proyectos').DataTable().destroy();
          this.proyectos = resp.proyectos;
          this.chRef.detectChanges();
          const table: any = $('#table_proyectos');
          this.dataTable = table.DataTable({
              language: {
                search: 'Buscar:',
                lengthMenu: 'Mostrar   _MENU_   proyectos',
                zeroRecords: 'No hay proyectos',
                info: 'Mostrar página _PAGE_ de _PAGES_',
                infoEmpty: 'No hay proyectos disponibles',
                infoFiltered: '(filtrado desde _MAX_ registros)',
                paginate: {
                  previous: 'Anterior',
                  next: 'Siguiente'
                }
              }
          });

        } else {
          console.log(resp);
        }
      }
    );
  }

  onEditar(p) {
    console.log(p);
    $('#modalProyecto').modal({backdrop: 'static', keyboard: false});
    this.PROYECTO_SERVICE.getProyecto(p.idProyecto).subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          console.log(resp);
          this.proyectoSeleccionado = resp.proyecto;
          console.log(this.proyectoSeleccionado);
        } else {
          console.log(resp);
        }
      }
    );
  }
  onSubmitProyecto(formProyecto) {
    $('#modalProyecto').modal('hide');
    this.PROYECTO_SERVICE.updateHoursProyecto(this.proyectoSeleccionado.codProyecto
      , this.proyectoSeleccionado.horaIngreso, this.proyectoSeleccionado.horaSalida
      , this.USUARIO_SERVICE.usuario.usuario).subscribe( (resp: any) => {
       if (resp.codigoRespuesta === '0') {
        this.getProyectos();
        console.log(resp);
        toastr.success('', 'Se actualizo las horas del proyecto correctamente.');

     } else {
       console.log(resp);
     }
    });
  }

  onConfirmarFinalizar(p) {
    $('#modalConfirm').modal({backdrop: 'static', keyboard: false});
    this.proyectoSeleccionado = p;
  }

  onFinalizar() {
    $('#modalConfirm').modal('hide');
    this.PROYECTO_SERVICE.finalizeProyecto(this.proyectoSeleccionado.codProyecto
      , this.USUARIO_SERVICE.usuario.usuario).subscribe( (resp: any) => {
       if (resp.codigoRespuesta === '0') {
        this.getProyectos();
        console.log(resp);
        toastr.success('', 'Se finalizo  el proyecto correctamente.');

     } else {
       console.log(resp);
     }
    });
  }


}
