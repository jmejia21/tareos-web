import { Component, OnInit, OnDestroy, ViewChild, ElementRef, HostListener } from '@angular/core';
import * as XLSX from 'xlsx';
import { Proyecto } from '../../models/proyecto.model';
import { UsuarioService, PerfilService, ProyectoService } from '../../services/service.index';
import { SidebarService } from '../../services/shared/sidebar.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-upload-file-parte',
  templateUrl: './upload-file-parte.component.html',
  styles: []
})
export class UploadFileParteComponent implements OnInit, OnDestroy {

  constructor(public USUARIO_SERVICE: UsuarioService, public PERFIL_SERVICE: PerfilService
            , public SIDEBAR_SERVICE: SidebarService
            , public PROYECTO_SERVICE: ProyectoService) {
            }


  public proyectosFile: Proyecto[] = [];
  public lstProyectosFile: any = [];

  public actividadesFile: any[] = [];
  public lstActividadesFile: any[] = [];


  public partidasFile: any[] = [];
  public lstPartidasFile: any[] = [];

  public date: any;
  public dateSelected: Date;


  public nameFile: any;
  public nameFileMacro: any;

  public titleFile = 'Escoger archivo';
  public titleFileMacro = 'Escoger archivo';


  public proyecto: any;
  public tareadores = [];
  public tareadorMatch: any;
  public tareadoresMatch: any = [];

  public tareador: any;

  public usuarios: any[] = [];
  public lstUsuarios: any[] = [];

  public firstFormEnabled = true;
  public secondFormEnabled = false;

  public desProyectoSelecionado = '';

  public message: any;
  public messages: any[] = [];


  public trabajadores = [];
  public lstTrabajadores = [];


  public files: FileList;

  public result: any;
  public results: any[] =  [];

  public weekMacro = 0;
  public weekMacroNumber = 0;
  public weeksMacros = [];

  public periodoMacro = '';
  public periodoWeek = '';

  public periodosMacros = [];

  //
  public partes = [];
  public lstPartes = [];


  public formsData: FormData[] = [];
  public formData: FormData = new FormData();

  public tituloCodParte = '';

  private unsubscribe = new Subject<void>();

  @ViewChild('archivo')
  myInputVariable: ElementRef;

  @HostListener('window:beforeunload', ['$event'])
  beforeunloadHandler(event) {
   event.returnValue = `¿Está seguro de salir de la pagina?`;
  }

  ngOnInit() {
    $('#datepicker-autoclose').datepicker({
      format: 'dd-mm-yyyy',
      autoclose: true,
      todayHighlight: true,
      language: 'es'
    }).on('changeDate', (ev) => {
      const dateUTC = new Date(ev.date.getTime() + (ev.date.getTimezoneOffset() * 60000));
      this.dateSelected = dateUTC;
      this.date = this.getStringDate(dateUTC);
    });
    this.proyecto = { codProyecto: '', descProyecto: '' };
    this.tareadorMatch = { nombreCompleto: '', apellidos: '', nombres: '', dni: '', idRol: '', categoria: '' };
    this.date = null;
    this.message = { valid: false, show: false, class: '', message: '' };
    this.result = { proyecto: '' , tareador: '', archivo: '' };
    this.tareador = null;
  }

  ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

  onFileChangeExcel(evt: any) {

    const target: DataTransfer = (evt.target) as DataTransfer;
    if (target.files.length === 1) {
      this.nameFile = target.files[0].name;
      this.titleFile = target.files[0].name;

      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        /* read workbook */
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        const wsname: string = wb.SheetNames[0];
        const ws: XLSX.WorkSheet = wb.Sheets[wsname];
        // console.log(wsname);
        XLSX.utils.sheet_to_json(ws, { header: 1 });
        this.onReadExcel(XLSX.utils.sheet_to_json(ws, { header: 1 }));
      };
      reader.readAsBinaryString(target.files[0]);
    } else {
      this.nameFile = '';
      this.titleFile = 'Escoger archivo';
    }


  }

  onReadExcel(json: any) {
    this.trabajadores = [];
    for (let index = 0; index < json.length; index++) {
      const data = json[index];
      if (index >= 1 && data.length > 0) {
        const names = data[1].split(', ');
        if ( (data[4] === '003' || data[4] === '001') && data[1] === data[3] )  {
          if (!this.tareadores.includes({ apellidos: names[0], nombres: names[1], dni: data[0], idRol: data[4], categoria: data[5] })) {
            this.tareadores.push({ apellidos: names[0], nombres: names[1], dni: data[0], idRol: data[4], categoria: data[5] });
          }
        }
        if ( data[4] === '001' || data[4] === '003' || data[4] === '004' || data[4] === '005' || data[4] === '000' )  {
        this.trabajadores.push({ apellidos: names[0], nombres: names[1], dni: data[0].trim(), idRol: data[4], categoria: data[5] });
        }
      }
    }
  }


  onFileChangeMacro(evt: any) {
    this.message = { valid: true, show: false, class: 'alert-warning', message: ''};
    const target: DataTransfer = (evt.target) as DataTransfer;
    this.files = target.files;
    if (target.files.length > 0) {
    this.nameFileMacro = '';
    this.titleFileMacro = '';
    this.tituloCodParte = '';
    this.usuarios = [];
    this.actividadesFile = [];
    this.partidasFile = [];
    this.proyectosFile = [];
    this.partes = [];

    // arrays de procesamiento
    this.results = [];
    this.messages = [];
    this.weeksMacros = [];
    this.lstTrabajadores = [];
    this.lstProyectosFile = [];
    this.lstPartidasFile = [];
    this.lstActividadesFile = [];
    this.lstUsuarios = [];
    this.formsData = [];
    this.lstPartes = [];

    // tslint:disable-next-line:no-unused-expression
    for (let i = 0; i < this.files.length; i++) {
      const f = this.files[i];

      if (i + 1 === this.files.length) {
       this.nameFileMacro += f.name ;
       this.titleFileMacro += f.name ;
      } else {
       this.nameFileMacro += f.name + ',';
       this.titleFileMacro += f.name + ',';
      }

      const reader: FileReader = new FileReader();
      reader.onload = (e: any) => {
        this.result = { proyecto: '' , tareador: '', archivo: '' };
        this.result.archivo = f.name ;
        const bstr: string = e.target.result;
        const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });
        // tslint:disable-next-line:prefer-for-of
        for (let index = 0; index < wb.SheetNames.length; index++) {
          const element = wb.SheetNames[index];
          const wsname: string = wb.SheetNames[index];
          const ws: XLSX.WorkSheet = wb.Sheets[wsname];
          switch (wsname) {
            case 'TAREO':
              this.onReadUsuario(XLSX.utils.sheet_to_json(ws, { header: 1 }));
              break;
            case 'Proyecto':
              this.onReadProyecto(XLSX.utils.sheet_to_json(ws, { header: 1 }));
              break;
            case 'Actividad':
              this.onReadActividad(XLSX.utils.sheet_to_json(ws, { header: 1 }));
              break;
            default:
              break;
          }
        }
      };
      reader.readAsBinaryString(target.files[i]);
      reader.onloadend = (e: any) => {
        this.results.push(this.result);
        this.messages.push(this.message);
        this.weeksMacros.push(this.weekMacro);
        this.lstTrabajadores.push(this.trabajadores);
        this.lstProyectosFile.push(this.proyectosFile);
        this.lstPartidasFile.push(this.partidasFile);
        this.lstActividadesFile.push(this.actividadesFile);
        this.lstUsuarios.push(this.usuarios);
        this.formsData.push(this.formData);
        this.periodosMacros.push(this.periodoMacro);
        this.lstPartes.push(this.partes);
        this.tareadoresMatch.push(this.tareadorMatch);
      };

    }

    } else {
      this.nameFileMacro = '';
      this.titleFileMacro = 'Escoger archivo';
    }

  }

  onReadUsuario(json: any) {
    this.usuarios = [];
    let weekHalf = 0;
    let weekNumber = 0;
    for (let index = 0; index < json.length; index++) {
      const data = json[index];
      if (index === 1 && data.length > 0) {
        this.desProyectoSelecionado = data[11].substring(12);
        this.tituloCodParte = data[11];
      }
      if (index === 2 && data.length > 0) {
       console.log(data[11].substring(13).split('(')[0].trim());
       const periodo = data[11].substring(13).split('(')[0].trim();
       if (periodo.includes('_')) {
         const periodoSplit = periodo.split('_');
         weekNumber = + periodoSplit[0];
         weekHalf = + periodoSplit[1];
      } else {
          weekHalf = 0;
          weekNumber = + data[11].substring(13).split('(')[0].trim();
       }

       this.weekMacro = weekNumber;
       this.weekMacroNumber = weekHalf;
       this.periodoMacro = data[11].substring(7, 16).split('-')[0].trim() + data[11].substring(7, 16).split('-')[1].trim();
      }
      if (index >= 7 && data.length > 0) {
        // console.log(data);
        if (data[2] && data[3]) {
          this.usuarios.push({ dni: data[2], nombre: data[3] , codProyecto: '', dniSup: ''});
          this.partes.push({ codParte: '', codProyecto: '', dni:  data[2], periodo: '', flagPeriodo: ''
          , usuarioRegistro: this.USUARIO_SERVICE.usuario.usuario
          , usuarioModificacion: this.USUARIO_SERVICE.usuario.usuario });

        }
      }
    }

    this.makeMatchTrabajadores_Tareadores();

  }

  onReadProyecto(json: any) {
    json.forEach(data => {
      if (data.length > 0) {
        const found = this.proyectosFile.some(item => item.codProyecto ===  data[0]);
        if (!found) {
          this.proyectosFile.push(new Proyecto(data[0], data[1], data[2]));
        }
      }
    });

    for (const pf of this.proyectosFile) {
      if (pf.descProyecto === this.desProyectoSelecionado) {
        this.result.proyecto = pf.descProyecto;
        this.proyecto = { codProyecto: pf.codProyecto, descProyecto: pf.descProyecto };
        break;
      }
    }
  }

  onReadActividad(json: any) {
    json.forEach(data => {
      if (data.length > 0) {
        const foundAct = this.actividadesFile.some(item => (item.codProyecto === data[0].substring(0, 5) && item.codPartida === data[1]));
        if (!foundAct) {
          this.actividadesFile.push({
            codFinal: data[0], codProyecto: data[0].substring(0, 5)
            , codPartida: data[1], descActividad: data[2]
          });
      }
        const found = this.partidasFile.some(item => item.codPartida ===  data[1]);
        if (!found) {
          this.partidasFile.push({ codPartida: data[1], descPartida: data[2] });
        }
      }
    });
  }

  onValidateTareador(): boolean {
    let isFound = false;
    for (const usuario of this.usuarios) {
      if (usuario.dni === this.tareador.dni) {
        isFound = true;
        break;
      }
    }
    return isFound;
  }

  makeMatchTrabajadores_Tareadores() {
    this.tareadorMatch = null;
    for (const u of this.usuarios) {
      for (const t of this.tareadores) {
        if (u.dni === t.dni) {
          this.tareadorMatch = t;
          this.tareadorMatch.nombreCompleto = t.apellidos + ', ' + t.nombres;
          this.result.tareador = t.apellidos + ', ' + t.nombres;
          break;
        }
      }
    }
  }

  onMostrar() {
    // console.log(this.date);
    // console.log(this.getWeekNumber(this.dateSelected));
    this.firstFormEnabled = false;
    this.secondFormEnabled = true;
  }

  onEnableFirstForm() {
    this.firstFormEnabled = true;
  }

  onMacros() {
    if (this.files.length > 0) {
      $('#modalConfirm').modal({backdrop: 'static', keyboard: false});
    }
  }

   async onProcesar() {
    $('#modalConfirm').modal('hide');
    console.log(this.lstUsuarios);
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.results.length; i++) {
      const r =  this.results[i];
      console.log('r:' + i, r);
      this.messages[i] = { valid: true, show: true, class: 'alert-warning', message: 'Espere por favor...'};

      if (this.getWeekNumber(this.dateSelected) === this.weeksMacros[i]) {
        await Promise.all(
       [this.processTrabajadores(this.lstTrabajadores[i], this.USUARIO_SERVICE.usuario.usuario
          , this.USUARIO_SERVICE.usuario.usuario),
        this.processProyectos(this.lstProyectosFile[i], this.USUARIO_SERVICE.usuario.usuario
          , this.USUARIO_SERVICE.usuario.usuario),
          this.processPartidas(this.lstPartidasFile[i], this.USUARIO_SERVICE.usuario.usuario
          , this.USUARIO_SERVICE.usuario.usuario)]).then((async (firstResponse: any) => {
            if (firstResponse[0].codigoRespuesta === '0'
              && firstResponse[1].codigoRespuesta === '0'
              && firstResponse[2].codigoRespuesta === '0') {
              console.log('r1:' + i, r);
              await Promise.all(
             [this.processUsuarioProyectos(this.getValuesFromUsers(this.lstUsuarios[i], i), this.USUARIO_SERVICE.usuario.usuario
                , this.USUARIO_SERVICE.usuario.usuario),
              this.processActividades(this.lstActividadesFile[i], this.USUARIO_SERVICE.usuario.usuario
                , this.USUARIO_SERVICE.usuario.usuario)]).then(((secondResponse: any) => {
                  console.log('r2:' + i, r);

                  if (secondResponse[0].codigoRespuesta === '0'
                    && secondResponse[1].codigoRespuesta === '0') {

                  this.postPart(this.getValuesFromParts(this.lstPartes[i])).then((thirdResponse: any) => {
                    if (thirdResponse.codigoRespuesta === '0') {
                      this.messages[i] = {
                        valid: true, show: true, class: 'alert-success', message: 'Subida del archivo '
                          + this.results[i].archivo + ' satisfactoriamente.'
                      };
                      this.saveInFTP(this.files[i]);
                      this.SIDEBAR_SERVICE.notificacion.emit();
                  // contador de trabajdores no registrados
                  // confirmacion de salida
                    }
                  });
                  } else {

                    if (secondResponse[0].codigoRespuesta !== '0') {
                      this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'Se encontro el siguiente error: '
                      + ' ' + secondResponse[0].mensajeRespuesta + ' en el archivo ' + this.results[i].archivo  };
                    }

                    if (secondResponse[1].codigoRespuesta !== '0') {
                      this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'Se encontro el siguiente error: '
                      + ' ' + secondResponse[1].mensajeRespuesta + ' en el archivo ' + this.results[i].archivo  };
                    }
                  }
                })).catch(error => {
                  this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'Se encontro el siguiente error: '
                      + error.error.mensajeRespuesta  + ', en el archivo ' + this.results[i].archivo  };
                });
            } else {
              if (firstResponse[0].codigoRespuesta !== '0') {
                this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'Se encontro el siguiente error: '
                + ' ' + firstResponse[0].mensajeRespuesta + ' en el archivo ' + this.results[i].archivo  };
              }

              if (firstResponse[1].codigoRespuesta !== '0') {
                this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'Se encontro el siguiente error: '
                + ' ' + firstResponse[1].mensajeRespuesta + ' en el archivo ' + this.results[i].archivo  };
              }

              if (firstResponse[2].codigoRespuesta !== '0') {
                this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'Se encontro el siguiente error: '
                + ' ' + firstResponse[2].mensajeRespuesta + ' en el archivo ' + this.results[i].archivo  };
              }

            }
          })).catch(error => {
            this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'Se encontro el siguiente error: '
                + error.error.mensajeRespuesta  + ', en el archivo ' + this.results[i].archivo  };
          });
      }  else {
        this.messages[i] = { valid: false, show: true, class: 'alert-danger', message: 'La semana' + '(' + this.weeksMacros[i] + ')'
        + ' del parte ' + this.results[i].archivo
              + ' no coincide con la semana' + '(' + this.getWeekNumber(this.dateSelected) + ')'
              + ' de la fecha seleccionada del calendario.'  };
      }
    }

  }
  getValuesFromUsers(usuarios, i) {

    for (const u of usuarios) {
      u.codProyecto = this.proyecto.codProyecto;
      u.dniSup = this.tareadoresMatch[i].dni;
    }

    return usuarios;
  }

  getValuesFromParts(partes) {

    for (const p of partes) {
      p.codProyecto = this.proyecto.codProyecto;
      // p.codParte = 'codParte';
      p.codParte = this.tituloCodParte;
      p.periodo = this.periodoMacro;
      p.flagPeriodo = '' + this.weekMacroNumber;
    }

    return partes;
  }

  onCancelar() {
    this.nameFileMacro = '';
    this.titleFileMacro = 'Escoger archivo';
    this.myInputVariable.nativeElement.value = '';
    this.results = [];
    this.messages = [];
    this.weeksMacros = [];
    this.lstTrabajadores = [];
    this.lstProyectosFile = [];
    this.lstPartidasFile = [];
    this.lstActividadesFile = [];
    this.lstUsuarios = [];
    this.usuarios = [];
    this.formsData = [];
    this.periodosMacros = [];
    this.partes = [];
    this.lstPartes = [];
    this.tareadoresMatch = [];
  }

  private processTrabajadores(tareadores, usuarioRegistro, usuarioModificacion) {
    return new Promise((resolve, reject) => {
      this.USUARIO_SERVICE.processTrabajadores(tareadores, usuarioRegistro, usuarioModificacion)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(success => {
        resolve(success);
      }, error => {
        reject(error);
      });
    });
  }

  private processProyectos(proyectos, usuarioRegistro, usuarioModificacion) {
    return new Promise((resolve, reject) => {
      this.USUARIO_SERVICE.processProyectos(proyectos, usuarioRegistro, usuarioModificacion)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(success => {
        resolve(success);
      }, error => {
        reject(error);
      });
    });
  }

  private processPartidas(partidas, usuarioRegistro, usuarioModificacion) {
    return new Promise((resolve, reject) => {
      this.USUARIO_SERVICE.processPartidas(partidas, usuarioRegistro, usuarioModificacion)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(success => {
        resolve(success);
      }, error => {
        reject(error);
      });
    });
  }

  private processUsuarioProyectos(usuarioProyectos, usuarioRegistro, usuarioModificacion) {
    return new Promise((resolve, reject) => {
      this.USUARIO_SERVICE.processUsuarioProyectos(usuarioProyectos, usuarioRegistro, usuarioModificacion)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(success => {
        resolve(success);
      }, error => {
        reject(error);
      });
    });
  }

  private processActividades(actividades, usuarioRegistro, usuarioModificacion) {
    return new Promise((resolve, reject) => {
      this.USUARIO_SERVICE.processActividades(actividades, usuarioRegistro, usuarioModificacion)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(success => {
        resolve(success);
      }, error => {
        reject(error);
      });
    });
  }
  private postPart(partes) {
    return new Promise((resolve, reject) => {
      this.USUARIO_SERVICE.postPart(partes)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe(success => {
        resolve(success);
      }, error => {
        reject(error);
      });
    });
  }

  getStringDate(date: Date) {
    const dd = date.getDate();
    let ds = '';
    const mm = date.getMonth() + 1;
    let ms = '';
    const yyyy = date.getFullYear();
    if (dd < 10) { ds = '0' + dd; } else { ds = dd.toString(); }
    if (mm < 10) { ms = '0' + mm; } else { ms = mm.toString(); }
    const fechaFormato = ds + '-' +  ms + '-' + yyyy;
    return fechaFormato;
  }

  getStringDate_2(date: Date) {
    const dd = date.getDate();
    let ds = '';
    const mm = date.getMonth() + 1;
    let ms = '';
    const yyyy = date.getFullYear();
    if (dd < 10) { ds = '0' + dd; } else { ds = dd.toString(); }
    if (mm < 10) { ms = '0' + mm; } else { ms = mm.toString(); }
    const fechaFormato = ds + '/' +  ms + '/' + yyyy;
    return fechaFormato;
  }

 getWeekNumber(d: any) {
  d.setHours(0, 0, 0);
  d.setDate(d.getDate() + 4 - (d.getDay() || 7 ));
  const d1: any =  new Date(d.getFullYear(), 0, 1);
  return Math.ceil((((d - d1) / 8.64e7) + 1 ) / 7);

}

 saveInFTP(file) {
  const formData: FormData = new FormData();
  formData.append('file', file );
  formData.append('fecha', this.getStringDate_2(this.dateSelected) );
  // console.log(formData.get('file'));
  // console.log(formData.get('fecha'));
  this.USUARIO_SERVICE.uploadInFTP(formData)
  .pipe(takeUntil(this.unsubscribe))
  .subscribe(
    (resp: any) => {
      if (resp.codigoRespuesta === '0') {
          console.log(resp);
      } else {
        console.log(resp);
      }
    }
  );
 }

}

