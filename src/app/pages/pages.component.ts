import { Component, OnInit } from '@angular/core';
declare function init_plugins();
declare var jquery: any;
declare var $: any;
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: []
})
export class PagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  init_plugins();
  }

}
