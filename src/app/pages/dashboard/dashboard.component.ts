import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  public saldos = [];
  public invoices = [];
  public invoicesPorCobrar = [];
  public pagos = [];




  public barChartOptions: any = {
    scaleShowVerticalLines: false
    , responsive: true
  };

  public barChartLabels: string[] =  [];

  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [];

  public showBarChart = false;

  public date: any;


  constructor() { }

  ngOnInit() {

  }


}
