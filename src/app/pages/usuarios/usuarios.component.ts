import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UsuarioService, PerfilService } from '../../services/service.index';
import { SidebarService } from '../../services/shared/sidebar.service';
declare var jquery: any;
declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styles: []
})
export class UsuariosComponent implements OnInit {


  public tituloUsuario = '';

  public usuarios = [];
  public dataTable: any;
  public perfiles = [];
  public perfil: any;
  public sexo = '1';
  public usuario: any ;
  public pass: any;
  public apePaterno: any;
  public apeMaterno: any;
  public nombres: any;
  public dni: any;
  public email: any;
  public telefono: any;
  public idPerfil: any;
  public opciones: any;

  public usuarioRegistro: any;
  public idUsuario = '0';

  public selectedAll: any;

  public menu: any;
  public menuUsuario: any;

  public usuarioSeleccionado: any;

  constructor(public USUARIO_SERVICE: UsuarioService, public PERFIL_SERVICE: PerfilService
            , public SIDEBAR_SERVICE: SidebarService, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getUsuarios();
  }

  getUsuarios() {
    this.USUARIO_SERVICE.getUsuarios('usuario', 'asc').subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          $('#table_usuarios').DataTable().destroy();
          this.usuarios = resp.usuarios;
          this.chRef.detectChanges();
          const table: any = $('#table_usuarios');
          this.dataTable = table.DataTable({
              language: {
                search: 'Buscar:',
                lengthMenu: 'Mostrar   _MENU_   usuarios',
                zeroRecords: 'No hay usuarios',
                info: 'Mostrar página _PAGE_ de _PAGES_',
                infoEmpty: 'No hay usuarios disponibles',
                infoFiltered: '(filtrado desde _MAX_ registros)',
                paginate: {
                  previous: 'Anterior',
                  next: 'Siguiente'
                }
              }
          });

        } else {
          console.log(resp);
        }
      }
    );
  }

  onMostrarNuevoUsuario() {
    this.tituloUsuario = 'Nuevo Usuario';
    this.usuario = null;
    this.pass = null;
    this.apePaterno = null;
    this.apeMaterno = null;
    this.nombres = null;
    this.dni = null;
    this.email = null;
    this.telefono = null;
    this.sexo = '1';

    this.idUsuario = '0';
    $('#modalUsuario').modal({backdrop: 'static', keyboard: false});
    this.getPerfiles();
    this.perfil = null;
  }

  getPerfiles() {
    this.PERFIL_SERVICE.getPerfiles().subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          this.perfiles =  resp.perfiles;
          console.log(this.perfiles);
        } else {
          console.log(resp);
        }
      }
    );
  }

  onSubmitUsuario(formUsuario) {
  console.log(formUsuario);
  if (this.idUsuario === '0') {
    this.USUARIO_SERVICE.addUsuario(this.usuario, this.pass, this.apePaterno, this.apeMaterno, this.nombres,
      this.dni, this.sexo, this.email, this.telefono, this.perfil.codPerfil, this.USUARIO_SERVICE.usuario.usuario
      ).subscribe( (resp: any) => {
       if (resp.codigoRespuesta === '0') {
         this.getUsuarios();
         console.log(resp);
         toastr.success('Usuario', 'Se registro correctamente.');
         $('#modalUsuario').modal('hide');

     } else {
       console.log(resp);
     }
  });
  } else {
    this.USUARIO_SERVICE.updateUsuario(this.idUsuario, this.usuario, this.pass, this.apePaterno, this.apeMaterno, this.nombres,
      this.dni, this.sexo, this.email, this.telefono, this.perfil.codPerfil, this.USUARIO_SERVICE.usuario.usuario, this.opciones
      ).subscribe( (resp: any) => {
       if (resp.codigoRespuesta === '0') {
         this.getUsuarios();
         console.log(resp);
         toastr.success('', 'Se modifico correctamente.');
         $('#modalUsuario').modal('hide');

     } else {
       console.log(resp);
     }
    });

  }
  }

  onMostrarFrmAcceso(u) {
    // this.usuarioSeleccionado = u;
    this.selectedAll = false;
    this.menu = this.SIDEBAR_SERVICE.menu;
    for (const m of this.menu) {
      m.selected = false;
      if (m.submenu) {
        for (const sm of m.submenu) {
          sm.selected = false;
        }
      }
    }

    $('#modalAcceso').modal({backdrop: 'static', keyboard: false});
    this.USUARIO_SERVICE.getUsuario(u.idUsuario).subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
            this.usuarioSeleccionado = resp.usuario;
            this.menuUsuario = this.SIDEBAR_SERVICE.cargarMenuUsuarioPorOpciones(resp.usuario.opciones);
            for (const m of this.menu) {
              let isFound = false;
              for (const mu of this.menuUsuario) {
                if (m.id === mu.id) {
                isFound = true;
                break;
                }
              }
              if (isFound) {
                m.selected = true;
              }

              if (m.submenu) {
                for (const sm of m.submenu) {
                  for (const mu of this.menuUsuario) {
                    if (mu.submenu) {
                      isFound = false;
                      for (const smu of mu.submenu) {
                        if ( sm.id === smu.id  ) {
                          isFound = true;
                          break;
                        }
                        if (isFound) {
                          break;
                        }
                      }
                      if (isFound) {
                        sm.selected = true;
                      }
                      if ( (m.id === mu.id) && m.submenu.length !== mu.submenu.length) {
                        m.selected = false;
                      }
                      if ( (m.id === mu.id) && m.submenu.length === mu.submenu.lengt) {
                        m.selected = true;
                      }
                    }
                  }
                }
              }
            }
          }
      });
  }

  onSelectAll() {
    for (const m of this.menu) {
      m.selected = this.selectedAll;
      if (m.submenu) {
        for (const sm of m.submenu) {
          sm.selected = this.selectedAll;
        }
      }
    }
  }

  checkIfAllSelected(m) {
    this.selectedAll = this.menu.every((item: any) => {
      return item.selected === true;
    });
    if (m.submenu && m.selected) {
      for (const sm of m.submenu) {
        sm.selected = true;
      }
    }
    if (m.submenu && !m.selected) {
     for (const sm of m.submenu) {
        sm.selected = false;
      }
    }
  }

  checkIfAllSubMenuSelected(m) {
    m.selected = m.submenu.every((item: any) => {
      return item.selected === true;
    });
  }

  onMostrarEditar(u) {
    this.tituloUsuario = 'Editar Usuario';

    console.log(u);

    this.USUARIO_SERVICE.getUsuario(u.idUsuario).subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          this.idUsuario = resp.usuario.idUsuario;
          this.usuario = resp.usuario.usuario;
          this.pass = resp.usuario.pass;
          this.apePaterno = resp.usuario.apePaterno;
          this.apeMaterno = resp.usuario.apeMaterno;
          this.nombres = resp.usuario.nombres;
          this.dni = resp.usuario.dni;
          this.email = resp.usuario.email;
          this.telefono = resp.usuario.telefono;
          this.opciones = resp.usuario.opciones;
          this.sexo = resp.usuario.sexo;
          $('#modalUsuario').modal({backdrop: 'static', keyboard: false});
          this.PERFIL_SERVICE.getPerfiles().subscribe(
            (resp2: any) => {
              if (resp2.codigoRespuesta === '0') {
                this.perfiles =  resp2.perfiles;
                for (const p of this.perfiles) {
                  if (p.idPerfil === resp.usuario.idPerfil) {
                    this.perfil = p;
                    break;
                  }
                }
                console.log(this.perfiles);
              } else {
                console.log(resp2);
              }
            }
          );

        } else {
          console.log(resp);
        }
      }
    );
  }

  onEliminar(u) {
    $('#modaEliminar').modal({backdrop: 'static', keyboard: false});

    this.USUARIO_SERVICE.getUsuario(u.idUsuario).subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          this.idUsuario = resp.usuario.idUsuario;
        } else {
          console.log(resp);
        }
      }
    );

  }

  onActivar(u) {
    $('#modalActivar').modal({backdrop: 'static', keyboard: false});

    this.USUARIO_SERVICE.getUsuario(u.idUsuario).subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          this.idUsuario = resp.usuario.idUsuario;
        } else {
          console.log(resp);
        }
      }
    );

  }


  onConfirmacionEliminar() {
    $('#modaEliminar').modal('hide');
    this.USUARIO_SERVICE.updateStateUsuario(this.idUsuario, this.USUARIO_SERVICE.usuario.usuario, '1').subscribe((resp2: any) => {
      if (resp2.codigoRespuesta === '0') {
        toastr.success('Se elimino correctamente.', 'Usuario');
        this.getUsuarios();
        console.log(resp2);

      } else {
        console.log(resp2);
      }
    });
  }

  onConfirmacionActivar(){
    $('#modalActivar').modal('hide');
    this.USUARIO_SERVICE.updateStateUsuario(this.idUsuario, this.USUARIO_SERVICE.usuario.usuario, '2').subscribe((resp2: any) => {
      if (resp2.codigoRespuesta === '0') {
        toastr.success('Se activo correctamente.', 'Usuario');
        this.getUsuarios();
        console.log(resp2);
      } else {
        console.log(resp2);

      }
    });
  }

  onSubmitAcceso(f) {
    let opciones = '';
    for (const menu of this.menu) {
      if ( menu.selected && !menu.submenu) {
          opciones += menu.id + ',';
      }
      if (menu.submenu) {
        let atLeastOneIsChecked = false;
        for (const submenu of menu.submenu) {
          if (submenu.selected) {
            atLeastOneIsChecked = true;
            break;
          }
        }
        if (atLeastOneIsChecked) {
          opciones += menu.id + ',';
        }
        for (const submenu of menu.submenu) {
          if (submenu.selected) {
            opciones += submenu.id + ',';
          }
        }
      }
    }
    opciones = opciones.substring(0, opciones.length - 1);
    this.usuarioSeleccionado.opciones = opciones;


    this.USUARIO_SERVICE.updateUsuarioOpciones(this.usuarioSeleccionado.idUsuario,
                         this.usuarioSeleccionado.opciones, this.USUARIO_SERVICE.usuario.usuario
      ).subscribe( (resp: any) => {
       if (resp.codigoRespuesta === '0') {
         this.getUsuarios();
         toastr.success('', 'Se modifico los accesos correctamente.');
         $('#modalAcceso').modal('hide');

     } else {
       console.log(resp);
     }
    });
  }

}
