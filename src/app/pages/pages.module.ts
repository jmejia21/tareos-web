import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';



import { PAGES_ROUTES } from './pages.routes';
// Pipe
import { PipesModule } from '../pipes/pipes.module';
import { CommonModule } from '@angular/common';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UploadFileParteComponent } from './upload-file-parte/upload-file-parte.component';
import { AsistenciasComponent } from './asistencias/asistencias.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { PartidaControlComponent } from './partida-control/partida-control.component';
import { TrabajadoresComponent } from './trabajadores/trabajadores.component';
import { TrabajadoresNoRegistradosComponent } from './trabajadores-no-registrados/trabajadores-no-registrados.component';


@NgModule({
    declarations: [
        DashboardComponent,
        UsuariosComponent,
        UploadFileParteComponent,
        AsistenciasComponent,
        ProyectosComponent,
        PartidaControlComponent,
        TrabajadoresComponent,
        TrabajadoresNoRegistradosComponent,
    ],
    exports: [
        DashboardComponent
    ],
    imports : [
        CommonModule,
        SharedModule,
        PAGES_ROUTES,
        FormsModule,
        PipesModule,

    ]
})
export class PagesModule { }
