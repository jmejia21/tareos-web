import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UsuarioService } from '../../services/service.index';
import { SidebarService } from '../../services/shared/sidebar.service';

declare var jquery: any;
declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-asistencias',
  templateUrl: './asistencias.component.html',
  styleUrls: ['./asistencias.component.css']
})
export class AsistenciasComponent implements OnInit {

  public dataTable: any;
  public asistencias = [];

  constructor(public USUARIO_SERVICE: UsuarioService, public SIDEBAR_SERVICE: SidebarService
            , private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getDisapproved();
  }

  getDisapproved() {
    this.USUARIO_SERVICE.getDisapproved().subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          $('#table_aprobaciones').DataTable().destroy();
          this.asistencias = resp.asistencias;
          this.chRef.detectChanges();
          const table: any = $('#table_aprobaciones');
          this.dataTable = table.DataTable({
                  language: {
                    search: 'Buscar:',
                    lengthMenu: 'Mostrar   _MENU_   asistencias',
                    zeroRecords: 'No hay asistencias por aprobar',
                    info: 'Mostrar página _PAGE_ de _PAGES_',
                    infoEmpty: 'No hay asistencias disponibles',
                    infoFiltered: '(filtrado desde _MAX_ registros)',
                    paginate: {
                      previous: 'Anterior',
                      next: 'Siguiente'
                    }
                  },
                  columnDefs: [
                    {
                       targets: 0,
                       checkboxes: {
                          selectRow: true
                       }
                    }
                 ],
                 select: {
                    style: 'multi'
                 },
                 order: [[1, 'asc']]
                });
        }
      });
  }

  onSubmit(form) {
    const selected = this.dataTable.column(0).checkboxes.selected();
    if (selected.length > 0) {
     $('#modalConfirm').modal({backdrop: 'static', keyboard: false});
    } else {
      toastr.warning('Debe seleccionar al menos una opción.', '');
    }
  }

  onAprobar() {
    const ids = [];
    const selected = this.dataTable.column(0).checkboxes.selected();
    if (selected.length > 0) {
      for (const s of selected) {
        ids.push(s);
      }
      this.USUARIO_SERVICE.updateAproval(ids, this.USUARIO_SERVICE.usuario.usuario).subscribe( (resp2: any) => {
        if (resp2.codigoRespuesta === '0') {
         this.SIDEBAR_SERVICE.notificacion.emit(ids);
         toastr.success('Se aprobo correctamente.', '');
         this.getDisapproved();
      } else {
        toastr.warning(resp2.mensajeRespuesta, '');
      }
        $('#modalConfirm').modal('hide');
     });
    }

  }

}
