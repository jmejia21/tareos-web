import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UsuarioService } from '../../services/service.index';

declare var jquery: any;
declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-partida-control',
  templateUrl: './partida-control.component.html',
  styles: []
})
export class PartidaControlComponent implements OnInit {

  public partidas = [];
  public dataTable: any;

  constructor(public USUARIO_SERVICE: UsuarioService, private chRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.getPartidas();
  }

  getPartidas() {
    this.USUARIO_SERVICE.getPartida().subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
          $('#table_partidas').DataTable().destroy();
          this.partidas = resp.partidas;
          this.chRef.detectChanges();
          const table: any = $('#table_partidas');
          this.dataTable = table.DataTable({
              language: {
                search: 'Buscar:',
                lengthMenu: 'Mostrar   _MENU_   partidas',
                zeroRecords: 'No hay partidas',
                info: 'Mostrar página _PAGE_ de _PAGES_',
                infoEmpty: 'No hay partidas disponibles',
                infoFiltered: '(filtrado desde _MAX_ registros)',
                paginate: {
                  previous: 'Anterior',
                  next: 'Siguiente'
                }
              }
          });

        } else {
          console.log(resp);
        }
      }
    );
  }


}
