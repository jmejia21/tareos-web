import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { UploadFileParteComponent } from './upload-file-parte/upload-file-parte.component';
import { AsistenciasComponent } from './asistencias/asistencias.component';
import { PartidaControlComponent } from './partida-control/partida-control.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { TrabajadoresComponent } from './trabajadores/trabajadores.component';
import { TrabajadoresNoRegistradosComponent } from './trabajadores-no-registrados/trabajadores-no-registrados.component';
import { AccesoGuardGuard } from '../services/guards/acceso-guard.guard';
import { UploadFileParteCanDeactivateGuardGuard } from '../services/guards/upload-file-parte-can-deactivate-guard.guard';

const pagesRoutes: Routes = [
     { path: 'dashboard', component: DashboardComponent, data: {titulo: 'Dashboard', id: '1'},  canActivate: [AccesoGuardGuard]},
     { path: 'usuarios', component: UsuariosComponent, data: {titulo: 'Usuarios', id: '2'},  canActivate: [AccesoGuardGuard]},
     { path: 'trabajadores', component: TrabajadoresComponent, data: {titulo: 'Trabajadores', id: '4'},  canActivate: [AccesoGuardGuard]},
     { path: 'proyectos', component: ProyectosComponent, data: {titulo: 'Proyectos', id: '5'},  canActivate: [AccesoGuardGuard]},
     { path: 'partida-control', component: PartidaControlComponent, data: {titulo: 'Partida de control', id: '6'}
          ,  canActivate: [AccesoGuardGuard]},
     { path: 'upload-file-parte', component: UploadFileParteComponent, data: {titulo: 'Subida de partes', id: '8'}
     ,  canActivate: [AccesoGuardGuard], canDeactivate: [UploadFileParteCanDeactivateGuardGuard] },
     { path: 'asistencias', component: AsistenciasComponent, data: {titulo: 'Asistencias', id: '10'},  canActivate: [AccesoGuardGuard]},

     { path: 'trabajadores-no-registrados',
     component: TrabajadoresNoRegistradosComponent, data: { titulo: 'Trabajadores no registrados', id: '11' }
     , canActivate: [AccesoGuardGuard] },

     { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];

export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes );
