import { Component, OnInit } from '@angular/core';
import { SidebarService, UsuarioService } from '../../services/service.index';
declare var jquery: any;
declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: []
})
export class SidebarComponent implements OnInit {

  public userName: string;
  public data: any;
  public pendientes = '';
  public trabajadores = '';

  constructor(public USUARIO_SERVICE: UsuarioService, public SIDEBAR_SERVICE: SidebarService) {
    }
  ngOnInit() {
    this.SIDEBAR_SERVICE.cargarMenuUsuario();
    this.getPendientes();
    this.getTrabajadorNoRegistrado();

    this.SIDEBAR_SERVICE.notificacion.subscribe(
      resp => {
        console.log('Notificar', resp);
        this.getPendientes();
        this.getTrabajadorNoRegistrado();
      });
  }

  getPendientes() {
    this.USUARIO_SERVICE.getAssitancePending().subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
            this.pendientes = resp.asistencia;
        } else {
          console.log(resp);
        }
      }
    );
  }

  getTrabajadorNoRegistrado() {
    this.USUARIO_SERVICE.getUnknownWorkers().subscribe(
      (resp: any) => {
        if (resp.codigoRespuesta === '0') {
            this.trabajadores = resp.trabajador;
        } else {
          console.log(resp);
        }
      }
    );
  }

  onResize() {
    $('body').removeClass('sidebar-toggled');
    $('#accordionSidebar').removeClass('toggled');

  }



}
