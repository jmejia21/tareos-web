import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {  UsuarioService } from '../../services/service.index';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styles: []
})
export class HeaderComponent implements OnInit {

  constructor(public router: Router, public USUARIO_SERVICE: UsuarioService) { }

  ngOnInit() {
  }

  logout() {
    this.USUARIO_SERVICE.logout();
  }

}
