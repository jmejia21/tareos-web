import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';


@Injectable()
export class LoginGuardGuard implements CanActivate {


  constructor(
    public USUARIO_SERVICE: UsuarioService,
    public router: Router) {

  }

  canActivate() {
    if (this.USUARIO_SERVICE.estaLogueado()) {
        return true;
    } else {
      console.log('NO PASO EL GUARD');
      this.router.navigate(['/login']);
      return false;
    }

  }
}
