import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import { SidebarService } from '../shared/sidebar.service';

@Injectable()
export class AccesoGuardGuard implements  CanActivate {
  constructor(
    public USUARIO_SERVICE: UsuarioService,
    public SIDEBAR_SERVICE: SidebarService,
    public router: Router) {
      this.SIDEBAR_SERVICE.cargarMenuUsuario();
  }

  canActivate(route: ActivatedRouteSnapshot) {
    const id = route.data.id ;
    let isFound = false;
    for (const m of this.SIDEBAR_SERVICE.menuUsuario) {
        if ( m.id === id) {
          isFound = true;
          break;
        }
        if (!isFound) {
          if (m.submenu) {
            for (const sm of m.submenu) {
              if ( sm.id === id) {
                isFound = true;
                break;
              }
            }
          }
        }

    }
    if (isFound) {
      return true;
    } else {
      console.log('NO PASO EL ACCESO GUARD');

      this.USUARIO_SERVICE.logout();
      return false;
    }
  }
}
