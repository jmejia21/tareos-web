import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import { Observable, from } from 'rxjs';
import { UploadFileParteComponent } from '../../pages/upload-file-parte/upload-file-parte.component';
import { ConfirmDialogService } from '../../pages/confirm-dialog/confirm-dialog.service';


@Injectable()
export class UploadFileParteCanDeactivateGuardGuard implements  CanDeactivate<UploadFileParteComponent> {

  constructor(private CONFIRM_DIALOG_SERVICE: ConfirmDialogService) { }

  canDeactivate(
    component: UploadFileParteComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean {
       let v = false;
       return from(this.CONFIRM_DIALOG_SERVICE.confirm().then((value) => {
            if (value) {
              v = true;
              return v;
            }
       }));
  }
}
