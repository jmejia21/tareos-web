import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { URL_SERVICIOS } from '../../config/config';
import { map } from 'rxjs/operators';
@Injectable()
export class UsuarioProyectoService {

  constructor(
    public http: HttpClient,
    public router: Router) {
  }

  finilizeProject(codProyecto: string, usuarioModificacion: string ) {
    return this.http.post(URL_SERVICIOS + 'userproj/finalize',
        { codProyecto, usuarioModificacion})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

  asignUsuarioProyecto(usuarioSup: string, codProyecto: string, usuarioRegistro: string, usuarios: [] ) {
    return this.http.post(URL_SERVICIOS + 'userproj/finalize',
        { usuarioSup, codProyecto, usuarioRegistro, usuarios})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

}
