import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { URL_SERVICIOS } from '../../config/config';
import { map } from 'rxjs/operators';

@Injectable()
export class ProyectoService {

  constructor(
    public http: HttpClient,
    public router: Router) {
  }

  getProyectos() {
    return this.http.get(URL_SERVICIOS + 'proyecto/busqueda');
   }
   getProyecto(idProyecto: number) {
    return this.http.get(URL_SERVICIOS + 'proyecto?id=' + idProyecto);
   }

   addProyecto(codProyecto: string, descProyecto: string, usuarioRegistro: string ) {
    return this.http.post(URL_SERVICIOS + 'proyecto/new',
        { codProyecto, descProyecto, usuarioRegistro})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

  updateProyecto(idProyecto: string, codProyecto: string, descProyecto: string, usuarioModificacion: string ) {
    return this.http.put(URL_SERVICIOS + 'proyecto/new',
        { idProyecto, codProyecto, descProyecto, usuarioModificacion})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

  updateStateProyecto(idProyecto: string, usuarioModificacion: string ) {
    return this.http.put(URL_SERVICIOS + 'proyecto/new',
        { idProyecto, usuarioModificacion})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

  updateHoursProyecto(codProyecto: string, horaIngreso: string, horaSalida: string, usuarioModificacion: string ) {
    return this.http.put(URL_SERVICIOS + 'proyecto/hours',
        { codProyecto, horaIngreso, horaSalida, usuarioModificacion})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

  finalizeProyecto(codProyecto: string, usuarioModificacion: string ) {
    return this.http.post(URL_SERVICIOS + 'empproj/finalize',
        { codProyecto, usuarioModificacion})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }
}
