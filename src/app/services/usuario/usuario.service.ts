import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { URL_SERVICIOS } from '../../config/config';
import { Usuario } from '../../models/usuario.model';
import { map } from 'rxjs/operators';

declare var toastr: any;

@Injectable()
export class UsuarioService {

  company: any = null;
  usuario: Usuario = null;
  token =  '';


  constructor(
    public http: HttpClient,
    public router: Router) {
      this.cargarStorage();
  }

  estaLogueado() {
    return this.usuario === null ? false : true;
  }

  cargarStorage() {
    if (localStorage.getItem('usuario')) {
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
      this.company = JSON.parse(localStorage.getItem('company'));

    } else {
      this.token = '';
      this.usuario = null;
      this.company = null;

    }
  }


  guardarStorage(token: string, usuario: Usuario ) {
    localStorage.setItem('usuario', JSON.stringify(usuario));
    this.usuario = usuario;

  }



  login(usuario: Usuario) {

      usuario.origen = '1';
      const url = URL_SERVICIOS + 'login';
      return this.http.post(url, usuario)
      .pipe(map((resp: any) => {
        console.log('resp:', resp);
        if ( resp.codigoRespuesta !== '0') {
          toastr.warning(resp.mensajeRespuesta, '');
          return false;
        } else {
            this.guardarStorage('', resp.usuario);
            return true;

        }
      }));
  }

  logout() {
    this.usuario = null;
    localStorage.removeItem('usuario');

    this.router.navigate(['/login']);
   }


   getUsuarios(sortBy: string, direction: string) {
    return this.http.get(URL_SERVICIOS + 'usuario/busqueda?sortBy=' + sortBy + '&direction=' + direction);
   }
   getUsuario(idUsuario: string) {
    return this.http.get(URL_SERVICIOS + 'usuario?idUsuario=' + idUsuario );
   }

   addUsuario(usuario: string, pass: string, apePaterno: string, apeMaterno: string, nombres: string, dni: string, sexo: string
            , email: string, telefono: string, idPerfil: string, usuarioRegistro: string ) {
    return this.http.post(URL_SERVICIOS + 'usuario/new',
        { usuario, pass, apePaterno, apeMaterno, nombres, dni, sexo
          ,          email, telefono, idPerfil, usuarioRegistro})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

  updateUsuario(idUsuario: string, usuario: string, pass: string, apePaterno: string, apeMaterno: string, nombres: string, dni: string,
                sexo: string, email: string, telefono: string, idPerfil: string, usuarioModificacion: string, opciones?: string ) {
    return this.http.put(URL_SERVICIOS + 'usuario',
        { idUsuario, usuario, pass, apePaterno, apeMaterno, nombres, dni,
          sexo, email, telefono, idPerfil, usuarioModificacion, opciones})
          .pipe(map((resp: any) => {
        return resp;
       }));
  }

  updateUsuarioOpciones(idUsuario: string, opciones: string , usuarioModificacion: string) {
return this.http.put(URL_SERVICIOS + 'usuario/options',
{ idUsuario, usuarioModificacion, opciones})
.pipe(map((resp: any) => {
return resp;
}));
}

  updateStateUsuario(idUsuario: string, usuarioModificacion: string , flag: string ) {
    return this.http.put(URL_SERVICIOS + 'usuario/estado?flag=' + flag,
    { idUsuario, usuarioModificacion})
     .pipe(map((resp: any) => {
    return resp;
    }));
  }

  processTrabajadores(trabajadores: any[], usuarioRegistro: string , usuarioModificacion: string ) {
    return this.http.post(URL_SERVICIOS + 'trabajador/all',
    { trabajadores, usuarioRegistro, usuarioModificacion})
     .pipe(map((resp: any) => {
    return resp;
    }));
  }

  processProyectos(proyectos: any[], usuarioRegistro: string , usuarioModificacion: string ) {
    return this.http.post(URL_SERVICIOS + 'proyecto/all',
    { proyectos, usuarioRegistro, usuarioModificacion})
     .pipe(map((resp: any) => {
    return resp;
    }));
  }

  processPartidas(partidas: any[], usuarioRegistro: string, usuarioModificacion: string) {
    return this.http.post(URL_SERVICIOS + 'partida/all',
      { partidas, usuarioRegistro, usuarioModificacion })
      .pipe(map((resp: any) => {
        return resp;
      }));
  }

  processUsuarioProyectos(trabajadorProyectos: any[], usuarioRegistro: string, usuarioModificacion: string) {
    return this.http.post(URL_SERVICIOS + 'empproj/all',
      { trabajadorProyectos, usuarioRegistro, usuarioModificacion })
      .pipe(map((resp: any) => {
        return resp;
      }));
  }

  processActividades(actividades: any[], usuarioRegistro: string, usuarioModificacion: string) {
    return this.http.post(URL_SERVICIOS + 'activity/all',
      { actividades, usuarioRegistro, usuarioModificacion })
      .pipe(map((resp: any) => {
        return resp;
      }));
  }

  // ASISTENCIAS

  getDisapproved() {
    return this.http.get(URL_SERVICIOS + 'assistance/disapproved');
   }

   updateAproval(asistencias: any[], usuarioModificacion: string) {
    return this.http.put(URL_SERVICIOS + 'assistance/approval',
      { asistencias, usuarioModificacion })
      .pipe(map((resp: any) => {
        return resp;
      }));
   }


  // PROYECTO

  getProjects() {
    return this.http.get(URL_SERVICIOS + 'proyecto/all');
   }

  // TRABAJADOR

  getWorkers() {
    return this.http.get(URL_SERVICIOS + 'trabajador/all');
   }

  // PARTIDA
  getPartida() {
    return this.http.get(URL_SERVICIOS + 'partida/all');
   }

  // ASISTENCIA
  getAssitancePending() {
    return this.http.get(URL_SERVICIOS + 'assistance/pending');
   }

    // TRABAJADOR NO REGISTRADO
  getUnknownWorkers() {
    return this.http.get(URL_SERVICIOS + 'trabajador/unknown');
   }

   getAllUnkwonWorkers() {
    return this.http.get(URL_SERVICIOS + 'trabajador/unknown/all');
   }


   uploadInFTP(file) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'multipart/form-data'
      })
    };
    return this.http.post(URL_SERVICIOS + 'ftp/upload',
     file )
    .pipe(map((resp: any) => {
      return resp;
    }));
   }

   // parte
   postPart(partes: []) {
    return this.http.post(URL_SERVICIOS + 'part',
      {partes})
      .pipe(map((resp: any) => {
        return resp;
      }));
   }



}
