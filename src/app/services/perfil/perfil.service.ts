import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { URL_SERVICIOS } from '../../config/config';


@Injectable()
export class PerfilService {



  constructor(
    public http: HttpClient,
    public router: Router) {
  }

   getPerfiles() {
    return this.http.get(URL_SERVICIOS + 'perfil/list');
   }





}
