import { Injectable, EventEmitter } from '@angular/core';
import { UsuarioService } from '../usuario/usuario.service';
import { ROLES } from '../../config/config';
import { Component, OnInit } from '@angular/core';

@Injectable()
export class SidebarService implements OnInit {
  public menu  = [
    {id: '1', icon: 'fa-tachometer-alt', nombre: 'Dashboard', url: '/dashboard', selected: false},
    {id: '2', icon: 'fa-user', nombre: 'Usuarios', url: '/usuarios', selected: false},
    {id: '3', icon: 'fa-cogs', nombre: 'Maestros', selected: false ,
                                  submenu: [{id: '4', nombre: 'Trabajadores', url: '/trabajadores', selected: false},
                                            {id: '11', nombre: 'Trabajadores no registrados'
                                            , url: '/trabajadores-no-registrados', selected: false},
                                            {id: '5', nombre: 'Proyectos', url: '/proyectos', selected: false},
                                            {id: '6', nombre: 'Partidas de control', url: '/partida-control', selected: false}
                                          ]},
    {id: '7', icon: 'fa-upload', nombre: 'Partes', selected: false
                                  , submenu: [{id: '8', nombre: 'Subir Archivo', url: '/upload-file-parte', selected: false}]},
    {id: '9', icon: 'fa-check', nombre: 'Aprobaciones', selected: false
                                  , submenu: [{id: '10', nombre: 'Asistencias', url: '/asistencias', selected: false}]}
];
  public menuUsuario: any;

  constructor(public USUARIO_SERVICE: UsuarioService) {
  }

  public notificacion = new EventEmitter<any>();


  ngOnInit() {
    this.cargarMenuUsuario();
  }

  cargarMenuUsuario() {
    let opciones = [];
    const meUsuario = [];
    if (this.USUARIO_SERVICE.usuario.opciones) {
      opciones = this.USUARIO_SERVICE.usuario.opciones.split(',');
      for (const m of this.menu) {
        // Menu sin submenu
        let isFoundW = false;
        for (const o of opciones) {
          if ( !m.submenu && m.id === o) {
            isFoundW = true;
            break;
          }
        }
        if (isFoundW) {
          meUsuario.push(m);
        }
        if (m.submenu) {
          const submenu = [];
          for (const sm of m.submenu) {
            isFoundW = false;
            for (const o of opciones) {
              if ( sm.id === o) {
                isFoundW = true;
                break;
              }
            }
            if (isFoundW) {
              submenu.push(sm);
            }
          }
          if (submenu.length > 0) {
            const me =  {id: m.id, icon: m.icon, nombre: m.nombre, selected: false ,
              submenu: []};
            me.submenu = submenu;
            meUsuario.push(me);
          }
        }
      }
    }
    this.menuUsuario = meUsuario;
  }

  cargarMenuUsuarioPorOpciones(opciones: string) {
    let op = [];
    const meUsuario = [];
    if (opciones) {
      op = opciones.split(',');
      for (const m of this.menu) {
        // Menu sin submenu
        let isFoundW = false;
        for (const o of opciones) {
          if ( !m.submenu && m.id === o) {
            isFoundW = true;
            break;
          }
        }
        if (isFoundW) {
          meUsuario.push(m);
        }
        if (m.submenu) {
          const submenu = [];
          for (const sm of m.submenu) {
            isFoundW = false;
            for (const o of op) {
              if ( sm.id === o) {
                isFoundW = true;
                break;
              }
            }
            if (isFoundW) {
              submenu.push(sm);
            }
          }
          if (submenu.length > 0) {
            const me =  {id: m.id, icon: m.icon, nombre: m.nombre, selected: false ,
                          submenu: []};
            me.submenu = submenu;
            meUsuario.push(me);
          }
        }
      }
    }
    return meUsuario;
  }

}
