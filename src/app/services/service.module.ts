import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';

import { ConfirmDialogService } from '../pages/confirm-dialog/confirm-dialog.service';

import {
  SharedService,
  LoginGuardGuard,
  AccesoGuardGuard,
  UploadFileParteCanDeactivateGuardGuard,
  UsuarioService,
  ProyectoService,
  UsuarioProyectoService,
  SidebarService,
  PerfilService,
  } from './service.index';
@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    ConfirmDialogService,
    SharedService,
    LoginGuardGuard,
    AccesoGuardGuard,
    UploadFileParteCanDeactivateGuardGuard,
    UsuarioService,
    ProyectoService,
    UsuarioProyectoService,
    SidebarService,
    PerfilService],
  declarations: []
})
export class ServiceModule { }
